public class Converter {
//Your names go here:
/*
* @Author: Mark Quintin
* Arjun Sivaprasadam
* John Matthew San Juan
*
*/
private double celsiusToFahrenheit(double C){
// TODO: The third student will implement this method
	double F = (C * 1.8) + 32;
return F;
}
private double fahrenheitToCelsius(double F){
 double C = (F - 32)/1.8;
return C ;
}
public static void main(String[] args) {
// Call CelsiusToFahrenheit to convert 180 Celsius to Fahrenheit value.
        Converter a = new Converter();
        double impVal;
        impVal = a.celsiusToFahrenheit(180);
        System.out.println("180� Celsius to Fahrenheit is: " + impVal);
// Call FahrenheitToCelsius to convert 250 Fahrenheit to Celsius value.
        double metVal;
        metVal = a.fahrenheitToCelsius(250);
        System.out.println("250� Fahrenheit to Celsius is: " + metVal);
	}
}
